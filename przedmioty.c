//Biblioteki
#include <stdio.h>
#include <stdlib.h>

//Struktura przechowująca informacje o dacie:
typedef struct DATA {
    int dzien;
    int miesiac;
    float godzina;
} Data;

typedef struct DANE {
    int id;
    int nauczyciel_id;
    char* nazwa;
    Data start;
    Data koniec;
} Dane;

//Struktura z listą dwukierunkową:
typedef struct PRZEDMIOT Przedmiot;

struct PRZEDMIOT {
    Dane dane;
    Przedmiot* next;
    Przedmiot* prev;
};

/**
 * Generowanie nowegu numeru id dla kolejnego dodanego elementu.
 * @author Jakub Książek
 * @param head
 */
int przypisz_id(Przedmiot *head) {
    Przedmiot *tmp;
    int key = 0;

    tmp = head;
    
    while (tmp != NULL) {
        if (tmp->dane.id > key) {
            key = (int) tmp->dane.id;
        }
        tmp = tmp->next;
    }
    
    return ++key;
}

/**
 * Stworzenie elementu na końcu listy.
 * @author Jakub Książek
 * @param head
 */
void stworz_element(Przedmiot **head) {
    Przedmiot *element;
    Data *start;
    Data *koniec;
    char bufor[30 + 1];
    char *nazwa;
    int id, dzien, miesiac;
    float godzina_roz, godzina_zak;

    system("cls");
    printf("Nazwa: ");
    gets(bufor);
    nazwa = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
    strcpy(nazwa, bufor);

    printf("Wpisz id wykladowcy: ");
    scanf("%i", &id);
    fflush(stdin);
    
    printf("Dzien: ");
    scanf("%i", &dzien);
    fflush(stdin);
    
    printf("Miesiac: ");
    scanf("%i", &miesiac);
    fflush(stdin);

    printf("Godzina rozpoczecia: ");
    scanf("%f", &godzina_roz);
    fflush(stdin);
    
    printf("Godzina zakonczenia: ");
    scanf("%f", &godzina_zak);
    fflush(stdin);
    
    element = (Przedmiot*) malloc(sizeof (Przedmiot));
    element->next = NULL;
    element->prev = NULL;
    element->dane.id = przypisz_id(*head);
    element->dane.nauczyciel_id = id;
    element->dane.nazwa = nazwa;
    element->dane.start.dzien = dzien;
    element->dane.start.miesiac = miesiac;
    element->dane.start.godzina = godzina_roz;
    element->dane.koniec.godzina = godzina_zak;    

    Przedmiot *pom, *tmp = element;

    if (*head == NULL)
        *head = tmp;
    else {
        pom = *head;
        while (pom->next != NULL)
            pom = pom->next;
        pom->next = tmp;
    }
}

void sortowanie_listy_po_id(Przedmiot **head) {
    Przedmiot *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Przedmiot*) malloc(sizeof (Przedmiot));
        tmp->dane.id = (*head)->dane.id;
        tmp->dane.nauczyciel_id = (*head)->dane.nauczyciel_id;
        tmp->dane.nazwa = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwa) + 1);
        strcpy(tmp->dane.nazwa, (*head)->dane.nazwa);
        tmp->dane.start = (*head)->dane.start;
        tmp->dane.koniec = (*head)->dane.koniec;
        
         tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (nowa->dane.id > tmp->dane.id) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && pom->next->dane.id < tmp->dane.id)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
         
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.nazwa)
            free(pom->dane.nazwa);
        free(pom);
    }
    *head = nowa;
}

void sortowanie_listy_po_dniu(Przedmiot **head) {
    Przedmiot *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Przedmiot*) malloc(sizeof (Przedmiot));
        tmp->dane.id = (*head)->dane.id;
        tmp->dane.nauczyciel_id = (*head)->dane.nauczyciel_id;
        tmp->dane.nazwa = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwa) + 1);
        strcpy(tmp->dane.nazwa, (*head)->dane.nazwa);
        tmp->dane.start = (*head)->dane.start;
        tmp->dane.koniec = (*head)->dane.koniec;
        
         tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (nowa->dane.start.dzien > tmp->dane.start.dzien) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && pom->next->dane.start.dzien < tmp->dane.start.dzien)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
         
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.nazwa)
            free(pom->dane.nazwa);
        free(pom);
    }
    *head = nowa;
}

void sortowanie_listy_po_miesiacu(Przedmiot **head) {
    Przedmiot *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Przedmiot*) malloc(sizeof (Przedmiot));
        tmp->dane.id = (*head)->dane.id;
        tmp->dane.nauczyciel_id = (*head)->dane.nauczyciel_id;
        tmp->dane.nazwa = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwa) + 1);
        strcpy(tmp->dane.nazwa, (*head)->dane.nazwa);
        tmp->dane.start = (*head)->dane.start;
        tmp->dane.koniec = (*head)->dane.koniec;
        
         tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (nowa->dane.start.miesiac > tmp->dane.start.miesiac) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && pom->next->dane.start.miesiac < tmp->dane.start.miesiac)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
         
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.nazwa)
            free(pom->dane.nazwa);
        free(pom);
    }
    *head = nowa;
}

void sortowanie_listy_po_godzinie(Przedmiot **head) {
    Przedmiot *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Przedmiot*) malloc(sizeof (Przedmiot));
        tmp->dane.id = (*head)->dane.id;
        tmp->dane.nauczyciel_id = (*head)->dane.nauczyciel_id;
        tmp->dane.nazwa = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwa) + 1);
        strcpy(tmp->dane.nazwa, (*head)->dane.nazwa);
        tmp->dane.start = (*head)->dane.start;
        tmp->dane.koniec = (*head)->dane.koniec;
        
         tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (nowa->dane.start.godzina > tmp->dane.start.godzina) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && pom->next->dane.start.godzina < tmp->dane.start.godzina)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
         
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.nazwa)
            free(pom->dane.nazwa);
        free(pom);
    }
    *head = nowa;
}

/**
 * Wyświetlanie listy.
 * @author Jakub Książek
 * @param head
 */
void wyswietlanie_listy(Przedmiot *head) {
    Przedmiot *tmp;

    system("cls");
    tmp = head;
    if (tmp == NULL)
        printf("Brak elementow");
    else {
        while (tmp != NULL) {
            printf("%i %i - zajecia z %s odbeda sie %i.%i w godzinach %.2f - %.2f\n", tmp->dane.id, tmp->dane.nauczyciel_id, tmp->dane.nazwa, tmp->dane.start.dzien, tmp->dane.start.miesiac, tmp->dane.start.godzina, tmp->dane.koniec.godzina);
            tmp = tmp->next;
        }
    }
    getchar();
}

/**
 * Szukanie elementu w liście.
 * @author Jakub Książek
 * @param head
 */
Przedmiot* wyszukaj_element(Przedmiot *head, int id) {
    Przedmiot *pom;

    pom = head;
    while (pom != NULL && pom->dane.id != id)
        pom = pom->next;

    return pom;
}

/**
 * Usuwanie elementu listy.
 * @author Jakub Książek
 * @param head
 */
void usun_element(Przedmiot **head) {
    Przedmiot *tmp, *pom;
    int id;

    system("cls");
    printf("Wpisz id przedmiotu: ");
    scanf("%i", &id);
    fflush(stdin);

    tmp = wyszukaj_element(*head, id);
    if (tmp == NULL)
        printf("Nie ma takiej osoby");
    else {
        if (tmp == *head) {
            *head = (*head)->next;
            if (tmp->dane.nazwa)
                free(tmp->dane.nazwa);
            free(tmp);
        } else {
            pom = *head;
            while ((strcmp(pom->next->dane.nazwa, tmp->dane.nazwa) != 0))
                pom = pom->next;
            pom->next = tmp->next;
            if (tmp->dane.nazwa)
                free(tmp->dane.nazwa);
            free(tmp);
        }
    }
}

/**
 * Zapis danych do pliku.
 * @author Jakub Książek
 * @param head
 */
void zapisz_do_pliku(Przedmiot *head) {
    FILE *zapisz = NULL;
    Przedmiot *tmp;
	char nazwa_pliku[] = "lista.txt";

    if (head == NULL)
        printf("Nie ma elementow do zapisania");
    else {
        zapisz = fopen(nazwa_pliku, "w");
        if (zapisz == NULL)
            printf("Blad otwarcia pliku");
        else {
            tmp = head;
            while (tmp != NULL) {
                fprintf(zapisz, "%i\n", tmp->dane.id);
                fprintf(zapisz, "%i\n", tmp->dane.nauczyciel_id);
                fprintf(zapisz, "%s\n", tmp->dane.nazwa);
                fprintf(zapisz, "%i\n", tmp->dane.start.dzien);
                fprintf(zapisz, "%i\n", tmp->dane.start.miesiac);
                fprintf(zapisz, "%.2f\n", tmp->dane.start.godzina);
                fprintf(zapisz, "%.2f\n", tmp->dane.koniec.godzina);
                tmp = tmp->next;
            }
            printf("Lista zostala zapisana");
        }
        fclose(zapisz);
    }
}

/**
 * Odczytywanie danych z pliku.
 * @author Jakub Książek
 * @param head
 */
void odczyt_z_pliku(Przedmiot **head) {
    FILE *wczytaj = NULL;
    char nazwa_pliku[] = "lista.txt";
    char bufor[30 + 1];
    int id, temp, dzien, miesiac;
    float godzina_roz, godzina_zak;    
    Przedmiot *tmp, *pom;

    wczytaj = fopen(nazwa_pliku, "r");
    if (wczytaj == NULL)
        printf("Blad otwarcia pliku");
    else {
        while (fscanf(wczytaj, "%d", &id) != EOF) {
            //alokowanie pamieci na nowy element
            tmp = (Przedmiot*) malloc(sizeof (Przedmiot));
            tmp->next = NULL;
            tmp->prev = NULL;
            //wczytywanie id
            tmp->dane.id = id;
            
            //wczytywanie nauczyciela
            fscanf(wczytaj, "%i", &temp);
            tmp->dane.nauczyciel_id = temp;
            
            
            //wczytywanie nazwy
            fscanf(wczytaj, "%s", bufor);
            tmp->dane.nazwa = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
            strcpy(tmp->dane.nazwa, bufor);
            
            //wczytywanie nauczyciela
            fscanf(wczytaj, "%i", &dzien);
            tmp->dane.start.dzien = dzien;
            tmp->dane.koniec.dzien = dzien;
            
            //wczytywanie nauczyciela
            fscanf(wczytaj, "%i", &miesiac);
            tmp->dane.start.miesiac = miesiac;
            tmp->dane.koniec.miesiac = miesiac;           
            
            //wczytywanie nauczyciela
            fscanf(wczytaj, "%f", &godzina_roz);
            tmp->dane.start.godzina = godzina_roz;
            
            //wczytywanie nauczyciela
            fscanf(wczytaj, "%f", &godzina_zak);
            tmp->dane.koniec.godzina = godzina_zak;             
            
            if (*head == NULL)
                *head = tmp;
            else {
                pom = *head;
                while (pom->next != NULL)
                    pom = pom->next;
                tmp->prev = pom;
                pom->next = tmp;
            }
        }
        printf("Plik zostal wczytany");
    }
    fclose(wczytaj);
}

//Menu główne

void menu(Przedmiot *head) {
    char i;

    do {
        system("cls");
        printf("1. Stworz nowy element\n");
        printf("2. Wyswietl liste\n");
        printf("4. Usun wybrany element\n");
        printf("5. Sortowanie\n");
        printf("z - zapisz liste\n");
        printf("w - wczytaj liste\n");
        printf("0. Wyjscie\n\n");
        printf("Twoj wybor: ");
        i = getchar();
        getchar();
        if (i == '1') stworz_element(&head);
        else if (i == '2') wyswietlanie_listy(head);
        else if (i == '3') sortowanie_listy_po_godzinie(&head);        
        else if (i == '4') usun_element(&head);
        else if (i == '5') sortowanie_listy_po_id(&head);
        else if (i == 'z') zapisz_do_pliku(head);
        else if (i == 'w') odczyt_z_pliku(&head);

    } while (i != '0');
}

int main() {
    Przedmiot *head = NULL;

    menu(head);
    return 0;
}
