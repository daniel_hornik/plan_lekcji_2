#ifndef FILESSERVICE_H
#define	FILESSERVICE_H

void openFile(GtkWidget *widget, gpointer okno)
{
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "Wystąpił błąd: %s", "nie można otworzyć pliku!");
    gtk_window_set_title(GTK_WINDOW(dialog), "Błąd");
    
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}


#endif	/* FILESSERVICE_H */

