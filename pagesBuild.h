#ifndef PAGESBUILD_H
#define	PAGESBUILD_H


void buildHome() {
    content         = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(vBox), content, FALSE, TRUE, 5);
  
    char *str = "<b><span font='20'>Plan lekcji</span></b>";
    title       = gtk_label_new(NULL);
    description = gtk_label_new(NULL);
    vSeparator  = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
   
    
    gtk_label_set_markup(GTK_LABEL(title), str);
    str = "<span font='14'>Autorzy:</span>\n<span font='11'>Jakub Książek\nDaniel Hornik\nBartłomiej Kubarek\nBartosz Cholewa\nMateusz Niewiara</span>";
    
    gtk_label_set_markup(GTK_LABEL(description), str);
    
    gtk_box_pack_start(GTK_BOX(content), title, FALSE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(content), vSeparator, FALSE, TRUE, 5);
    
    gtk_box_pack_start(GTK_BOX(content), description, FALSE, TRUE, 5);
}

#endif	/* PAGESBUILD_H */

