//Biblioteki
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Struktura z danymi wykladowcy
typedef struct DANE {
    int id;
    char* imie;
    char* nazwisko;
} Dane;

//Struktura z listą dwukierunkową
typedef struct WYKLADOWCA Wykladowca;

struct WYKLADOWCA {
    Dane dane;
    Wykladowca* next;
    Wykladowca* prev;
};

/**
 * Generowanie nowegu numeru id dla kolejnego dodanego elementu.
 * @author Jakub Książek
 * @param head
 */
int przypisz_id(Wykladowca *head) {
    Wykladowca *tmp;
    int key = 0;

    tmp = head;
    
    while (tmp != NULL) {
        if (tmp->dane.id > key) {
            key = (int) tmp->dane.id;
        }
        tmp = tmp->next;
    }
    
    return ++key;
}


/**
 * Stworzenie elementu na końcu listy.
 * @author Jakub Książek
 * @param head
 */
void stworz_element(Wykladowca **head) {
    Wykladowca *element;
    char bufor[30 + 1];
    char *imie, *nazwisko;
    int id;

    system("cls");
    printf("Imie: ");
    gets(bufor);
    imie = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
    strcpy(imie, bufor);

    printf("Nazwisko: ");
    gets(bufor);
    nazwisko = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
    strcpy(nazwisko, bufor);

    element = (Wykladowca*) malloc(sizeof (Wykladowca));
    element->next = NULL;
    element->prev = NULL;
    element->dane.id = przypisz_id(*head);
    element->dane.imie = imie;
    element->dane.nazwisko = nazwisko;

    Wykladowca *pom, *tmp = element;

    if (*head == NULL)
        *head = tmp;
    else {
        pom = *head;
        while (pom->next != NULL)
            pom = pom->next;
        pom->next = tmp;
    }
}

/**
 * Wyświetlanie listy.
 * @author Jakub Książek
 * @param head
 */
void wyswietlanie_listy(Wykladowca *head) {
    Wykladowca *tmp;

    system("cls");
    tmp = head;
    if (tmp == NULL)
        printf("Brak elementow");
    else {
        while (tmp != NULL) {
            printf("%i %s %s\n", tmp->dane.id, tmp->dane.imie, tmp->dane.nazwisko);
            tmp = tmp->next;
        }
    }
    getchar();
}

/**
 * Szukanie elementu w liście.
 * @author Jakub Książek
 * @param head
 */
Wykladowca* wyszukaj_element(Wykladowca *head, int id) {
    Wykladowca *pom;

    pom = head;
    while (pom != NULL && pom->dane.id != id)
        pom = pom->next;

    return pom;
}

/**
 * Czyszczenie listy.
 * @author Jakub Książek
 * @param head
 */
void usun_liste(Wykladowca **head) {
    Wykladowca *tmp;

    while (*head != NULL) {
        tmp = *head;
        *head = (*head)->next;
        if (tmp->dane.imie)
            free(tmp->dane.imie);
        if (tmp->dane.nazwisko)
            free(tmp->dane.nazwisko);
        free(tmp);
    }
}

/**
 * Usuwanie elementu listy.
 * @author Jakub Książek
 * @param head
 */
void usun_element(Wykladowca **head) {
    Wykladowca *tmp, *pom;
    int id;

    system("cls");
    printf("Wpisz id wykladowcy: ");
    scanf("%i", &id);
    fflush(stdin);

    tmp = wyszukaj_element(*head, id);
    if (tmp == NULL)
        printf("Nie ma takiej osoby");
    else {
        if (tmp == *head) {
            *head = (*head)->next;
            if (tmp->dane.imie)
                free(tmp->dane.imie);
            if (tmp->dane.nazwisko)
                free(tmp->dane.nazwisko);
            free(tmp);
        } else {
            pom = *head;
            while ((strcmp(pom->next->dane.nazwisko, tmp->dane.nazwisko) != 0))
                pom = pom->next;
            pom->next = tmp->next;
            if (tmp->dane.imie)
                free(tmp->dane.imie);
            if (tmp->dane.nazwisko)
                free(tmp->dane.nazwisko);
            free(tmp);
        }
    }
}

/**
 * Sortowanie listy danych po nazwisku nauczyciela.
 * @author Jakub Książek
 * @param head
 */
void sortowanie_listy_po_nazwisku(Wykladowca **head) {
    Wykladowca *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Wykladowca*) malloc(sizeof (Wykladowca));
        tmp->dane.imie = (char*) malloc(sizeof (char) * strlen((*head)->dane.imie) + 1);
        strcpy(tmp->dane.imie, (*head)->dane.imie);
        tmp->dane.nazwisko = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwisko) + 1);
        strcpy(tmp->dane.nazwisko, (*head)->dane.nazwisko);
        tmp->dane.id = (*head)->dane.id;
        tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (strcmp(nowa->dane.imie, tmp->dane.imie) > 0) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && strcmp(pom->next->dane.imie, tmp->dane.imie) < 0)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.imie)
            free(pom->dane.imie);
        if (pom->dane.nazwisko)
            free(pom->dane.nazwisko); 
        free(pom);
    }
    *head = nowa;
}

/**
 * Sortowanie listy danych po ID nauczyciela.
 * @author Jakub Książek
 * @param head
 */
void sortowanie_listy_po_id(Wykladowca **head) {
    Wykladowca *nowa = NULL, *tmp, *pom;

    while (*head != NULL) {
        tmp = (Wykladowca*) malloc(sizeof (Wykladowca));
        tmp->dane.imie = (char*) malloc(sizeof (char) * strlen((*head)->dane.imie) + 1);
        strcpy(tmp->dane.imie, (*head)->dane.imie);
        tmp->dane.nazwisko = (char*) malloc(sizeof (char) * strlen((*head)->dane.nazwisko) + 1);
        strcpy(tmp->dane.nazwisko, (*head)->dane.nazwisko);
        tmp->dane.id = (*head)->dane.id;
        tmp->next = NULL;
        if (nowa == NULL)
            nowa = tmp;
        else if (nowa->dane.id > tmp->dane.id) {
            tmp->next = nowa;
            nowa = tmp;
        } else {
            pom = nowa;
            while (pom->next != NULL && pom->next->dane.id < tmp->dane.id)
                pom = pom->next;
            tmp->next = pom->next;
            pom->next = tmp;
        }
        pom = *head;
        *head = (*head)->next;
        if (pom->dane.imie)
            free(pom->dane.imie);
        if (pom->dane.nazwisko)
            free(pom->dane.nazwisko); 
        free(pom);
    }
    *head = nowa;
}

/**
 * Zapis danych do pliku.
 * @author Jakub Książek
 * @param head
 */
void zapisz_do_pliku(Wykladowca *head) {
    FILE *zapisz = NULL;
    Wykladowca *tmp;
	char nazwa_pliku[] = "lista.txt";

    if (head == NULL)
        printf("Nie ma elementow do zapisania");
    else {
        zapisz = fopen(nazwa_pliku, "w");
        if (zapisz == NULL)
            printf("Blad otwarcia pliku");
        else {
            tmp = head;
            while (tmp != NULL) {
                fprintf(zapisz, "%i\n", tmp->dane.id);
                fprintf(zapisz, "%s\n", tmp->dane.imie);
                fprintf(zapisz, "%s\n", tmp->dane.nazwisko);
                tmp = tmp->next;
            }
            printf("Lista zostala zapisana");
        }
        fclose(zapisz);
    }
}

/**
 * Odczytywanie danych z pliku.
 * @author Jakub Książek
 * @param head
 */
void odczyt_z_pliku(Wykladowca **head) {
    FILE *wczytaj = NULL;
    char nazwa_pliku[] = "lista.txt";
    char bufor[30 + 1];
    int id;
    Wykladowca *tmp, *pom;

    wczytaj = fopen(nazwa_pliku, "r");
    if (wczytaj == NULL)
        printf("Blad otwarcia pliku");
    else {
        while (fscanf(wczytaj, "%d", &id) != EOF) {
            //alokowanie pamieci na nowy element
            tmp = (Wykladowca*) malloc(sizeof (Wykladowca));
            tmp->next = NULL;
            tmp->prev = NULL;
            //wczytywanie id
            tmp->dane.id = id;
            //wczytywanie imienia
            fscanf(wczytaj, "%s", bufor);
            tmp->dane.imie = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
            strcpy(tmp->dane.imie, bufor);
            //wczytywanie nazwiska
            fscanf(wczytaj, "%s", bufor);
            tmp->dane.nazwisko = (char*) malloc(sizeof (char)* (strlen(bufor) + 1));
            strcpy(tmp->dane.nazwisko, bufor);

            if (*head == NULL)
                *head = tmp;
            else {
                pom = *head;
                while (pom->next != NULL)
                    pom = pom->next;
                tmp->prev = pom;
                pom->next = tmp;
            }
        }
        printf("Plik zostal wczytany");
    }
    fclose(wczytaj);
}

//Menu główne

void menu(Wykladowca *head) {
    char i;

    do {
        system("cls");
        printf("1. Stworz nowy element\n");
        printf("2. Wyswietl liste\n");
        printf("3. Usun liste\n");
        printf("4. Usun wybrany element\n");
        printf("5. Sortowanie\n");
        printf("z - zapisz liste\n");
        printf("w - wczytaj liste\n");
        printf("0. Wyjscie\n\n");
        printf("Twoj wybor: ");
        i = getchar();
        getchar();
        if (i == '1') stworz_element(&head);
        else if (i == '2') wyswietlanie_listy(head);
        else if (i == '3') usun_liste(&head);
        else if (i == '4') usun_element(&head);
        else if (i == '5') sortowanie_listy_po_id(&head);
        else if (i == 'z') zapisz_do_pliku(head);
        else if (i == 'w') odczyt_z_pliku(&head);
    } while (i != '0');
}

int main() {
    Wykladowca *head = NULL;

    menu(head);
    return 0;
}


