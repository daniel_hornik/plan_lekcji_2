#ifndef DEFINES_H
#define	DEFINES_H

//Wersja aplikacji (string)
#define APP_VERSION "0.02"
//Implementacja typu boolean
#define true 1
#define false 0

GtkWidget *window;
GtkToolItem *homeButton;
GtkToolItem *editButton;
GtkToolItem *goBackButton;
GtkToolItem *goForwardButton;
GtkToolItem *openButton;
GtkToolItem *separator;
GtkToolItem *quitButton;
GtkWidget *vBox;
GtkWidget *content;
GtkWidget *toolbarContent;
GtkWidget *toolbar;
GtkWidget *title;  
GtkWidget *vSeparator;
GtkWidget *description;


int __OPENED_TAB = 0; //Aktualnie otwarta zakładka

enum {
    TEACHER_ID_COL = 0,
    TEACHER_FIRSTNAME_COL,
    TEACHER_SURNAME_COL,
    TEACHER_COLS_AMOUNT
};
enum {
    DATE_ID_COL = 0,
    DATE_DAY_COL,
    DATE_MONTH_COL,
    DATE_YEAR_COL,
    DATE_COLS_AMOUNT
};

enum {
    TAB_HOME_PAGE = 0,
    TAB_EDIT_HOME, //Wybór pola które chcemy edytować(nauczyciele, dni nauki, przedmioty)
    TAB_EDIT_TEACHER,
    TAB_EDIT_DATE,
    TAB_EDIT_SUBJECT
};

typedef struct _stack Stack;
/**
 *Stos przechowuje liczby DODATNIE
 */
struct _stack {
    int number;
    Stack *prev;
};

#endif	/* DEFINES_H */

