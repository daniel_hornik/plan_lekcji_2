#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include "defines.h"
#include "filesService.h"
#include "pagesBuild.h"
#include "signals.h"

Stack *history;
Stack *forwardHistory;

void openTeacher(GtkWidget *widget, gpointer data);
void openDates(GtkWidget *widget, gpointer data);
void openPageByNumber(int pageNumber);
void buildHome();
void buildTeacher();
void buildEditHome();




GtkWidget *firstname;
GtkWidget *surname;
GtkWidget *day;
GtkWidget *year;

int currentTeacherId = 0; //ID nauczyciela którego obecnie edytujemy, żeby było wiadomo dla kogo zapisać dane




/**
 * STOS
 */
void push(int number, Stack *stack) {
    Stack *newElement;
    
    newElement = malloc(sizeof(Stack));
    newElement->prev = stack->prev;
    newElement->number = stack->number;
    
    stack->prev = newElement;
    stack->number = number;
}

int pull(Stack *stack) {
    unsigned int returnNumber;
    Stack *temp;
    
    
    if (stack->number == -1) {
        stack->prev = NULL;
        return -1;
    }
    
    returnNumber = stack->number;
    temp = stack->prev;
    g_print("Wartosć w pull: %d", returnNumber);
    if (stack->prev == NULL) {        
        stack->number = -1;
    } else {
        stack->number = stack->prev->number;
        stack->prev = stack->prev->prev;
    }
    
    free(temp);
    return returnNumber;
}
void cleanStack(Stack *stack) {
    int temp;
    
    while ((temp = pull(stack)) != -1) {
        
    }
}

void registerHistory(int pageNumber) {
    cleanStack(forwardHistory);
    
    push(pageNumber, history);
}

void openForm(GtkWidget *widget, gpointer data) {
  
    
//#####ZROBIĆ SPRAWDZANIE CZY ISTNIEJE CONTENT######
    //gtk_container_remove(GTK_CONTAINER(vBox), content);
    int number;
    while((number = pull(history)) != -1) {
        g_print("%d", number);
    }
    
    //Jeśli nie można cofać to ustaw żeby zawsze można było cofnąć do głównej;
    if (number == -1)
        registerHistory(__OPENED_TAB);
    
    
     g_print("Danio");
}

void openPageByNumber(int pageNumber) {
    gtk_container_remove(GTK_CONTAINER(vBox), content);
    
    g_print("DEBUG: OTWIERAM: %d\n", pageNumber);
    
    switch(pageNumber) {
        case TAB_HOME_PAGE:
            buildHome();
            break;
        case TAB_EDIT_HOME:
            buildEditHome();
            break;
        case TAB_EDIT_TEACHER:
            buildTeacher();            
            break;
    }
    
    gtk_widget_show_all(window);
    __OPENED_TAB = pageNumber;
}

void backAction(GtkWidget *window, gpointer *data) {
    int prevPageNumber = pull(history);
    if (__OPENED_TAB == prevPageNumber) {
        return;
    }
    
    if (prevPageNumber == -1) {
        prevPageNumber = TAB_HOME_PAGE;
        push(TAB_HOME_PAGE, history);
        return;
    }
    
    
    g_print("WPISUJE DO FORWARD: %d\n", __OPENED_TAB);
    push(__OPENED_TAB, forwardHistory);
    
    
    openPageByNumber(prevPageNumber);
}

void forwardAction(GtkWidget *window, gpointer *data) {
    int nextPageNumber = pull(forwardHistory);
    
    g_print("WYCZYTUJE DO FORWARD: %d\n", nextPageNumber);
    if (nextPageNumber == -1)
        return;
    
    push(__OPENED_TAB, history);
    openPageByNumber(nextPageNumber);
}


/**
 * Podwójne kliknięcie na nauczyciela
 * @param treeview
 * @param path
 * @param col
 * @param userdata
 */
void teacherDoubleClick(GtkTreeView * treeview, GtkTreePath * path, GtkTreeViewColumn * col, gpointer userdata) {
    GtkTreeModel *teacherModel;
    GtkTreeIter iter;
   
    teacherModel = gtk_tree_view_get_model(treeview);
    if(gtk_tree_model_get_iter(teacherModel, &iter, path)) {
        gchar *_firstname;
        gchar *_surname;
        gint  *_id;
       
        gtk_tree_model_get(teacherModel, &iter, TEACHER_FIRSTNAME_COL, &_firstname, - 1);
        gtk_tree_model_get(teacherModel, &iter, TEACHER_SURNAME_COL, &_surname, - 1);
        gtk_tree_model_get(teacherModel, &iter, TEACHER_ID_COL, &_id, - 1);
        //g_print("%s %s (ID: %d)\n", _firstname, _surname, _id);
        gtk_entry_set_text (GTK_ENTRY(firstname), _firstname);
        gtk_entry_set_text (GTK_ENTRY(surname), _surname);
        
        currentTeacherId = (int)_id;
        g_free(_firstname);
        g_free(_surname);
        g_free(_id);
    }
}
/**
 * Buduje zakładkę strony głównej 
 * 
 * @param widget
 * @param okno
 */
void openHome(GtkWidget *widget, gpointer okno) {
    if (__OPENED_TAB == TAB_HOME_PAGE)
        return;
    registerHistory(__OPENED_TAB);
    
    
    gtk_container_remove(GTK_CONTAINER(vBox), content);
    buildHome();
    gtk_widget_show_all(window);
    __OPENED_TAB = TAB_HOME_PAGE;
    
    
    
} 
void buildEditHome() {
    GtkWidget *eHButton;
    GtkWidget *buttonGrid;
    GtkWidget *eHLabel;
    
    buttonGrid      = gtk_grid_new();
    content         = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(vBox), content);
    gtk_container_add(GTK_CONTAINER(content), buttonGrid);
    gtk_grid_set_row_spacing(GTK_GRID(buttonGrid), (gint)7);
    gtk_grid_set_column_homogeneous(GTK_GRID(buttonGrid),TRUE);  
    
    eHLabel = gtk_label_new("Wybierz co chcesz zrobić: ");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHLabel,          0, 0, 12, 1);
    
    eHButton = gtk_button_new_with_label("Dodaj/Edytuj nauczyciela");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHButton,         3, 1, 6, 1);
    g_signal_connect(G_OBJECT(eHButton), "clicked", G_CALLBACK(openTeacher), (gpointer)window);
    
    eHButton = gtk_button_new_with_label("Dodaj/Edytuj dni nauki");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHButton,         3, 2, 6, 1);
    g_signal_connect(G_OBJECT(eHButton), "clicked", G_CALLBACK(openDates), (gpointer)window);
    
    eHButton = gtk_button_new_with_label("Dodaj/Edytuj przedmioty");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHButton,         3, 3, 6, 1);
    
    eHLabel = gtk_label_new("");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHLabel,          0, 4, 12, 1);
    
    eHButton = gtk_button_new_with_label("powrót");
    gtk_grid_attach(GTK_GRID(buttonGrid), eHButton,         4, 5, 4, 1);    
    g_signal_connect(G_OBJECT(eHButton), "clicked", G_CALLBACK(openHome), (gpointer)window);
   
}

void openEditHome(GtkWidget *widget, gpointer data) {
    if (__OPENED_TAB == TAB_EDIT_HOME) 
        return;
    registerHistory(__OPENED_TAB);
    
    
    gtk_container_remove(GTK_CONTAINER(vBox), content);
    buildEditHome();
    gtk_widget_show_all(window);
    
    __OPENED_TAB = TAB_EDIT_HOME;
    
    
}

void buildTeacher() {    
    GtkWidget *hBox;
    GtkWidget *label;
    GtkWidget *table;
    GtkWidget *table2;
    GtkWidget *button;

    content         = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(vBox), content, FALSE, TRUE, 5);
    
    hBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    
    
    
    table = gtk_grid_new();
    
    gtk_grid_set_column_homogeneous(GTK_GRID(table),TRUE);   
    
    
        
    firstname = gtk_entry_new();
    surname = gtk_entry_new();
    
    
    GtkWidget *frame = gtk_frame_new ("Dodaj/Edytuj nauczyciela");
    
    gtk_container_add(GTK_CONTAINER(content), frame);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_grid_set_row_spacing(GTK_GRID(table), (gint)7);
    
    label = gtk_label_new("Dodaj nowego nauczyciela, podając jego imię oraz nazwisko:");
    gtk_grid_attach(GTK_GRID(table), label,       0, 0, 12, 1);
    
    label = gtk_label_new("Imię: ");
    gtk_grid_attach(GTK_GRID(table), label,       4, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table), firstname,   5, 1, 3, 1);    
    
    label = gtk_label_new("Nazwisko: ");
    gtk_grid_attach(GTK_GRID(table), label,       4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table), surname,     5, 2, 3, 1);  
    
    button = gtk_button_new_with_label("Zapisz");
    gtk_grid_attach(GTK_GRID(table), button,      5, 3, 3, 1);
    
    
    
    /**
     * LISTA
     */    
    GtkListStore *teacherModel;
    GtkTreeIter iter;
    GtkWidget *treeview;
    teacherModel = gtk_list_store_new(TEACHER_COLS_AMOUNT, G_TYPE_UINT, G_TYPE_STRING, G_TYPE_STRING);
    gtk_list_store_append(teacherModel, &iter);
    
    gtk_list_store_set(teacherModel, &iter, TEACHER_ID_COL, 1, TEACHER_FIRSTNAME_COL, "Michał", TEACHER_SURNAME_COL,( guint ) "Papacz", - 1 );
    gtk_list_store_append(teacherModel, &iter);
    gtk_list_store_set(teacherModel, &iter, TEACHER_ID_COL, 12, TEACHER_FIRSTNAME_COL, "Jan", TEACHER_SURNAME_COL,( guint ) "Kowalski", - 1 );
    
    treeview = gtk_tree_view_new();
    //gtk_container_add(GTK_CONTAINER(content), treeview);
    
   
    
    table2 = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(table2),TRUE);   
    
    gtk_container_add(GTK_CONTAINER(content), table2);
    gtk_grid_set_row_spacing(GTK_GRID(table2), (gint)7);
    
    label = gtk_label_new("");
    gtk_grid_attach(GTK_GRID(table2), label,       0, 0, 12, 1);
    gtk_grid_attach(GTK_GRID(table2), treeview,       3, 1, 6, 30);
    
    gtk_tree_view_set_model( GTK_TREE_VIEW( treeview ), GTK_TREE_MODEL( teacherModel ) );
    g_object_unref( G_OBJECT( teacherModel ) );
    
    GtkTreeViewColumn *kolumna[] = {
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new()
    };
    GtkCellRenderer *komorka[] = {
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new()
    };
   
    gtk_tree_view_column_set_title( kolumna[TEACHER_ID_COL], "Id" );
    gtk_tree_view_column_pack_start( kolumna[TEACHER_ID_COL], komorka[TEACHER_ID_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[TEACHER_ID_COL], GTK_CELL_RENDERER( komorka[TEACHER_ID_COL] ), "text", TEACHER_ID_COL);
    gtk_tree_view_append_column( GTK_TREE_VIEW( treeview ), kolumna[TEACHER_ID_COL] );
    
    gtk_tree_view_column_set_title( kolumna[TEACHER_FIRSTNAME_COL], "Imię" );
    gtk_tree_view_column_pack_start( kolumna[TEACHER_FIRSTNAME_COL], komorka[TEACHER_FIRSTNAME_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[TEACHER_FIRSTNAME_COL], GTK_CELL_RENDERER( komorka[TEACHER_FIRSTNAME_COL] ), "text", TEACHER_FIRSTNAME_COL);
    gtk_tree_view_append_column(GTK_TREE_VIEW( treeview ), kolumna[TEACHER_FIRSTNAME_COL]);
    
    gtk_tree_view_column_set_title( kolumna[TEACHER_SURNAME_COL], "Nazwisko" );
    gtk_tree_view_column_pack_start( kolumna[TEACHER_SURNAME_COL], komorka[TEACHER_SURNAME_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[TEACHER_SURNAME_COL], GTK_CELL_RENDERER( komorka[TEACHER_SURNAME_COL] ), "text", TEACHER_SURNAME_COL);
    gtk_tree_view_append_column(GTK_TREE_VIEW( treeview ), kolumna[TEACHER_SURNAME_COL]);
    
    
    g_signal_connect( treeview, "row-activated",( GCallback ) teacherDoubleClick, NULL );
}
void openTeacher(GtkWidget *widget, gpointer data) {
    if (__OPENED_TAB == TAB_EDIT_TEACHER)
        return;
    registerHistory(__OPENED_TAB);
    
    
    gtk_container_remove(GTK_CONTAINER(vBox), content);
    buildTeacher();
    
    gtk_widget_show_all(window);
    
    __OPENED_TAB = TAB_EDIT_TEACHER;
    
}
 

void buildDates() {   
    GtkWidget *hBox;
    GtkWidget *label;
    GtkWidget *table;
    GtkWidget *table2;
    GtkWidget *button;
    
    
    content         = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(vBox), content, FALSE, TRUE, 5);
    
    hBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    
    
    
    table = gtk_grid_new();
    
    gtk_grid_set_column_homogeneous(GTK_GRID(table),TRUE);   
    
    
        
    day = gtk_entry_new();
    year = gtk_entry_new();
    
    
    GtkWidget *frame = gtk_frame_new ("Dodaj/Edytuj dni nauki");
    
    gtk_container_add(GTK_CONTAINER(content), frame);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_grid_set_row_spacing(GTK_GRID(table), (gint)7);
    
    label = gtk_label_new("Dodaj nową datę:");
    gtk_grid_attach(GTK_GRID(table), label,       0, 0, 12, 1);
    
    label = gtk_label_new("Dzień: ");
    gtk_grid_attach(GTK_GRID(table), label,       4, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table), day,   5, 1, 3, 1);    
    
    label = gtk_label_new("Rok: ");
    gtk_grid_attach(GTK_GRID(table), label,       4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table), year,     5, 2, 3, 1);  
    
    button = gtk_button_new_with_label("Zapisz");
    gtk_grid_attach(GTK_GRID(table), button,      5, 3, 3, 1);
    
    
    
    /**
     * LISTA
     */    
    GtkListStore *dateModel;
    GtkTreeIter iter;
    GtkWidget *treeview;
    dateModel = gtk_list_store_new(DATE_COLS_AMOUNT, G_TYPE_UINT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    gtk_list_store_append(dateModel, &iter);
    
    gtk_list_store_set(dateModel, &iter, DATE_ID_COL, 1, DATE_DAY_COL, "03", DATE_MONTH_COL, "Marzec", DATE_YEAR_COL, "2015", - 1 );
    gtk_list_store_append(dateModel, &iter);
    gtk_list_store_set(dateModel, &iter, DATE_ID_COL, 2, DATE_DAY_COL, "12", DATE_MONTH_COL, "Kwietnia", DATE_YEAR_COL, "2015", - 1 );
    
    treeview = gtk_tree_view_new();
    //gtk_container_add(GTK_CONTAINER(content), treeview);
    
   
    
    table2 = gtk_grid_new();
    gtk_grid_set_column_homogeneous(GTK_GRID(table2),TRUE);   
    
    gtk_container_add(GTK_CONTAINER(content), table2);
    gtk_grid_set_row_spacing(GTK_GRID(table2), (gint)7);
    
    label = gtk_label_new("");
    gtk_grid_attach(GTK_GRID(table2), label,       0, 0, 12, 1);
    gtk_grid_attach(GTK_GRID(table2), treeview,       3, 1, 6, 30);
    
    gtk_tree_view_set_model( GTK_TREE_VIEW( treeview ), GTK_TREE_MODEL( dateModel ) );
    g_object_unref( G_OBJECT( dateModel ) );
    
    GtkTreeViewColumn *kolumna[] = {
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new(),
        gtk_tree_view_column_new()
    };
    GtkCellRenderer *komorka[] = {
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new(),
        gtk_cell_renderer_text_new()
    };
   
    gtk_tree_view_column_set_title( kolumna[DATE_ID_COL], "Id" );
    gtk_tree_view_column_pack_start( kolumna[DATE_ID_COL], komorka[DATE_ID_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[DATE_ID_COL], GTK_CELL_RENDERER( komorka[DATE_ID_COL] ), "text", DATE_ID_COL);
    gtk_tree_view_append_column( GTK_TREE_VIEW( treeview ), kolumna[DATE_ID_COL] );
    
    gtk_tree_view_column_set_title( kolumna[DATE_DAY_COL], "Dzień" );
    gtk_tree_view_column_pack_start( kolumna[DATE_DAY_COL], komorka[DATE_DAY_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[DATE_DAY_COL], GTK_CELL_RENDERER( komorka[DATE_DAY_COL] ), "text", DATE_DAY_COL);
    gtk_tree_view_append_column(GTK_TREE_VIEW( treeview ), kolumna[DATE_DAY_COL]);
    
    gtk_tree_view_column_set_title( kolumna[DATE_MONTH_COL], "Miesiąc" );
    gtk_tree_view_column_pack_start( kolumna[DATE_MONTH_COL], komorka[DATE_MONTH_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[DATE_MONTH_COL], GTK_CELL_RENDERER( komorka[DATE_MONTH_COL] ), "text", DATE_MONTH_COL);
    gtk_tree_view_append_column(GTK_TREE_VIEW( treeview ), kolumna[DATE_MONTH_COL]);
    
    gtk_tree_view_column_set_title( kolumna[DATE_YEAR_COL], "Rok" );
    gtk_tree_view_column_pack_start( kolumna[DATE_YEAR_COL], komorka[DATE_YEAR_COL], TRUE );
    gtk_tree_view_column_add_attribute( kolumna[DATE_YEAR_COL], GTK_CELL_RENDERER( komorka[DATE_YEAR_COL] ), "text", DATE_YEAR_COL);
    gtk_tree_view_append_column(GTK_TREE_VIEW( treeview ), kolumna[DATE_YEAR_COL]);
    
    
    g_signal_connect( treeview, "row-activated",( GCallback ) teacherDoubleClick, NULL );
}
void openDates(GtkWidget *widget, gpointer data) {
    if (__OPENED_TAB == TAB_EDIT_DATE)
        return;
    registerHistory(__OPENED_TAB);
    
    
    gtk_container_remove(GTK_CONTAINER(vBox), content);
    buildDates();
    
    gtk_widget_show_all(window);
    
    __OPENED_TAB = TAB_EDIT_DATE;
    
}
/**
 * Funkcja eventu aktywująca pliki
 * @param app
 * @param userData
 */
static void activate (GtkApplication *app, gpointer userData) {   
    
    //GtkWidget *hBox;
    //GtkWidget *aligmentBox;
    
    char applicationTitle[20] = "Plan lekcji ";
    strcat(applicationTitle, APP_VERSION);
    
    window = gtk_application_window_new(app);
    //gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    gtk_window_set_default_size(GTK_WINDOW(window), 700, 400);
    gtk_window_set_title(GTK_WINDOW(window), applicationTitle);
    
    
    vBox            = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    //content         = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    toolbarContent  = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    //hBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_add(GTK_CONTAINER(window), vBox);
    gtk_container_add(GTK_CONTAINER(vBox), toolbarContent);
    //gtk_container_add(GTK_CONTAINER(vBox), content);
    //
    toolbar = gtk_toolbar_new();
    
    /**
     * Strona GŁÓWNA
     */
    
    gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_ICONS);
    gtk_container_set_border_width(GTK_CONTAINER(vBox), 0);
    gtk_widget_set_margin_top(GTK_WIDGET(vBox), 0);
    
    homeButton          = gtk_tool_button_new_from_stock(GTK_STOCK_HOME);
    editButton          = gtk_tool_button_new_from_stock(GTK_STOCK_EDIT);
    openButton          = gtk_tool_button_new_from_stock(GTK_STOCK_OPEN);
    goBackButton        = gtk_tool_button_new_from_stock(GTK_STOCK_GO_BACK);   
    goForwardButton     = gtk_tool_button_new_from_stock(GTK_STOCK_GO_FORWARD);   
    quitButton          = gtk_tool_button_new_from_stock(GTK_STOCK_QUIT);
    
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), homeButton, -1);
    separator   = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), separator, -1);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), editButton, -1);    
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), openButton, -1);
    separator   = gtk_separator_tool_item_new();
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), separator, -1);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), goBackButton, -1);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), goForwardButton, -1);
    separator   = gtk_separator_tool_item_new();    
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), separator, -1);
    gtk_toolbar_insert(GTK_TOOLBAR(toolbar), quitButton, -1);
    
    //char * str = "<sub>Ten</sub> <tt>tekst</tt> <span underline='error'>zostaĹ‚</span> <span font='20'>napisany</span> <span bgcolor='#f00'>rĂłĹĽnymi</span> <b><i>stylami</i></b>.";
    
    
    gtk_box_pack_start(GTK_BOX(toolbarContent), toolbar, FALSE, TRUE, 5);
    
    
    //gtk_box_pack_start(GTK_BOX(aligmentBox), description, FALSE, TRUE, 5);
    
    g_signal_connect(G_OBJECT(homeButton), "clicked", G_CALLBACK(openHome), (gpointer)window);
    g_signal_connect(G_OBJECT(editButton), "clicked", G_CALLBACK(openEditHome), (gpointer)window);
    g_signal_connect(G_OBJECT(openButton), "clicked", G_CALLBACK(openFile), (gpointer)window);
    g_signal_connect(G_OBJECT(goBackButton), "clicked", G_CALLBACK(backAction), (gpointer)window);
    g_signal_connect(G_OBJECT(goForwardButton), "clicked", G_CALLBACK(forwardAction), (gpointer)window);
    
    buildHome();
    //buildNew();
    //buildEditHome();
     
    gtk_widget_show_all(window);
}

 

 
int main( int argc, char *argv[])
{
    GtkApplication *app;
    int appStatus;
    
    history = malloc(sizeof(Stack));
    history->number = 0;
    history->prev=NULL;
    forwardHistory = malloc(sizeof(Stack));
    forwardHistory->number = -1;
    forwardHistory->prev=NULL;
    
    
    app = gtk_application_new("org.gtk_example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
   
    appStatus = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    
    return appStatus;
}